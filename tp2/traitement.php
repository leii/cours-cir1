<?php
if(isset($_POST['ajouter'])){
  if(!empty($_POST['nom']) && !empty($_POST['prix']) && !empty($_POST['qte']) && !empty($_FILES['photo'])){

    $apresVirgule = explode('.', $_POST['prix']);
    if($_POST['prix'] > 0 && is_numeric($_POST['prix'])){
      if(!isset($apresVirgule[1]) || $apresVirgule[1] <= 99 && $apresVirgule[1] >= 0){
          $prix = $_POST['prix'];
      }
    }

    if(ctype_alnum($_POST['nom'])){
      $nom = $_POST['nom'];
    }

    if($_POST['qte'] >= 0){
      $qte = $_POST['qte'];
    }

    if($_FILES['photo']['type'] == 'image/jpg' || $_FILES['photo']['type'] == 'image/jpeg' || $_FILES['photo']['type'] == 'image/png'){
      $type = explode('/', $_FILES['photo']['type']);
      $lienImage = './photos/'.$_POST['nom'].'.'.$type[1];
      move_uploaded_file($_FILES['photo']['tmp_name'], $lienImage);
    }


    if(isset($prix) && isset($nom) && isset($qte) && isset($lienImage)){
      $productList = fopen('mesproduits.csv', 'a');
      $text = $nom.';'.$prix.';'.$lienImage;
      fwrite($productList, $text."\n");
    }
  }
}

header("Location: ajouter_produits.php");
