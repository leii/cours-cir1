<!DOCTYPE HTML>
<html>
  <head>
    <title>Epicerie</title>
    <meta charset="utf-8"/>
  </head>
  <body>
    <form action="traitement.php" method="POST" enctype="multipart/form-data">
      <input type="file" name="photo" id="photo"><label for="photo">Photo du produit</label>
      <br/>
      <input type="text" name="nom" id="nom" placeholder="Nom du produit"/><label for="nom">Nom du produit</label>
      <br/>
      <input type="number" min="0" step="0.01" name="prix" id="prix" placeholder="prix"><label for="prix">Prix du produit</label>
      <br/>
      <input type="number" min="0" step="1" name="qte" id="qte" placeholder="quantité"><label for="qte">quantité du produit</label>
      <br/>
      <br/>
      <input type="submit" name="ajouter" value="Ajouter le produit"/>
    </form>
  </body>
</html>
