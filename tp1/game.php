<?php

session_start();

//options
if(isset($_GET["valider"])){
  session_destroy();
  session_start();
  if(isset($_GET["columns"])){
    $_SESSION["columns"] = $_GET["columns"];
    if($_SESSION["columns"] < 0){
      $_SESSION["columns"] = -$_SESSION["columns"];
    }
  }
  if(isset($_GET["rows"])){
    $_SESSION["rows"] = $_GET["rows"];
    if($_SESSION["rows"] < 0){
      $_SESSION["rows"] = -$_SESSION["rows"];
    }
  }
  if(isset($_GET["auto"])){
    $_SESSION["auto"] = true;
  }
  else{
    $_SESSION["auto"] = false;
  }
  initialisation($_SESSION["rows"], $_SESSION["columns"]);
  header("Location: index.php");
}


if(!isset($_SESSION["rows"])){
  $_SESSION["rows"] = 20;
}
if(!isset($_SESSION["columns"])){
  $_SESSION["columns"] = 20;
}
if(!isset($_SESSION["auto"])){
  $_SESSION["auto"] = false;
}




//creation du plateau

function initialisation($row, $column){

  $_SESSION["generation"] = -1;

  for($i=0;$i<$column+2;$i++){
    for($j=0;$j<$row+2;$j++){
      if($j==0 || $j == $row+1 || $i == 0 || $i == $column+1){
        $array[$j][$i] = 2;
      }
      else{

      $valeur = rand(0,5);

      if($valeur == 0){
          $array[$j][$i] = 1;
        }
        else{
          $array[$j][$i] = 0;
        }

      }
    }
  }
  $_SESSION["GenerationA"] = $array;
}


//afichage du plateau
function affichagePlateau($row, $column){
  for($i=1;$i<$row-1;$i++){
    echo "<tr>";
    for($j=1;$j<$column-1;$j++){
      if($_SESSION["GenerationA"][$i][$j] == 1){
        echo "<td ><div id='vivant'></div></td>";
      }
      else{
          echo "<td><div id='mort'></div></td>";
      }
    }
    echo "</tr>";
  }
}


//generation d'apres
function GenrationB($row, $column){
  $_SESSION["generation"] += 1;
  for($i=1;$i<$column-1;$i++){
    for($j=1;$j<$row-1;$j++){
      $counterVivant = 0;
      //determination du nombre de voisin vivant

      if($_SESSION["GenerationA"][$j-1][$i+1] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j-1][$i] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j-1][$i-1] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j][$i-1] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j][$i+1] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j+1][$i+1] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j+1][$i] == 1){
        $counterVivant += 1;
      }
      if($_SESSION["GenerationA"][$j+1][$i-1] == 1){
        $counterVivant += 1;
      }

      //determination de l'etat de la cellule a la generation d'apres
      if($counterVivant == 3){
        $_SESSION["GenerationA"][$j][$i] = 1;
      }
      if($counterVivant >= 4){
        $_SESSION["GenerationA"][$j][$i] = 0;
      }
      if($counterVivant <= 1){
        $_SESSION["GenerationA"][$j][$i] = 0;
      }
    }
  }
}

//reinitialisation
if(!isset($_SESSION["GenerationA"])){
  initialisation($_SESSION["rows"], $_SESSION["columns"]+2);
}
else{
  GenrationB($_SESSION["rows"]+2, $_SESSION["columns"]+2);
}


//auto play
if($_SESSION["auto"] == true){
    echo "<meta http-equiv='refresh' content='1; url=./index.php'/>";
}
