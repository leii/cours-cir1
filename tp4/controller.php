<?php

$ini_array = parse_ini_file("secrets.ini",true);
session_start();

try {
   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
} catch (Exception $e) {
        exit('Impossible to connect to database.');
}

//connection's function
if(filter_input(INPUT_POST, "submit", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["connectionError"] = NULL;
  $_SESSION["role"] = NULL;
  $_SESSION["name"] = NULL;
  $_SESSION["id"] = NULL;
  if(filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS)){
    $login = (string)filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS);
    if(filter_input(INPUT_POST, "pw", FILTER_SANITIZE_SPECIAL_CHARS)){
      $pw = (string)filter_input(INPUT_POST, "pw", FILTER_SANITIZE_SPECIAL_CHARS);
      try{
        $query = "SELECT * FROM Users WHERE login = :log";
        $statements = $bdd->prepare($query);
        $statements->execute([":log" => $login]);
        foreach ($statements as $row) {
          if(password_verify($pw, $row["password"])){
            $_SESSION["role"] = $row["rank"];
            $_SESSION["id"] = $row["id"];
            $_SESSION["name"] = $row["login"];
            $_SESSION["selectedDate"] = date("Y-m-d");
          }
        }
      }catch(Exception $e){
        exit("Connection impossible");
      }
    }
  }
  header("Location: index.php");
}
if(filter_input(INPUT_POST, "submit", FILTER_SANITIZE_SPECIAL_CHARS) && !filter_input(INPUT_POST, "role", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["connectionError"] = "Error while connecting";

}
//Signin function

if(filter_input(INPUT_POST,"signIn", FILTER_SANITIZE_SPECIAL_CHARS)){
    if(filter_input(INPUT_POST,"name", FILTER_SANITIZE_SPECIAL_CHARS)){
        $name = (string)filter_input(INPUT_POST,"name", FILTER_SANITIZE_SPECIAL_CHARS);
        if(filter_input(INPUT_POST,"pw", FILTER_SANITIZE_SPECIAL_CHARS) == filter_input(INPUT_POST,"pwConfirm", FILTER_SANITIZE_SPECIAL_CHARS)){
            $pw = (string)filter_input(INPUT_POST,"pw", FILTER_SANITIZE_SPECIAL_CHARS);
            $pwHash = password_hash($pw,PASSWORD_DEFAULT);
            if(filter_input(INPUT_POST, "rank", FILTER_SANITIZE_SPECIAL_CHARS)){
                $rank = (string)filter_input(INPUT_POST, "rank", FILTER_SANITIZE_SPECIAL_CHARS);
                $query = "SELECT * FROM Users WHERE login = :name";
                $statements = $bdd->prepare($query);
                $statements->execute([":name"=>$name]);
                $nbre = 0;
                foreach($statements as $row){
                    if($row["login"] == $name){
                        $nbre += 1;
                    }
                }
                if($nbre == 0){
                    $query = "INSERT INTO Users (login,password, rank) VALUES(:login, :pw,:rank)";
                    $statements = $bdd->prepare($query);
                    $statements->execute([":login"=>$name,":pw"=>$pwHash,":rank"=>$rank]);
                    $query2 = "SELECT * FROM Users WHERE login = :log";
                    $statements2 = $bdd->prepare($query2);
                    $statements2->execute([":log" => $name]);
                    foreach ($statements2 as $row) {
                      if($row["login"] == $name){;
                        $_SESSION["role"] = $row["rank"];
                        $_SESSION["id"] = $row["id"];
                        $_SESSION["name"] = $row["login"];
                        $_SESSION["selectedDate"] = date("Y-m-d");
                      }
                    }
                }
            }
        }
    }
    header("location: index.php");
}

if(filter_input(INPUT_POST, "signIn", FILTER_SANITIZE_SPECIAL_CHARS) && !filter_input(INPUT_POST, "role", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["connectionError"] = "Error while Signing in";

}

//deconnection
if(filter_input(INPUT_POST, "deco", FILTER_SANITIZE_SPECIAL_CHARS)){
  session_destroy();
  header("Location: index.php");
}

//select a date

if(filter_input(INPUT_POST, "previousMonth", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["selectedDate"] = date("Y-m-d", strtotime($_SESSION["selectedDate"]." -1 month"));
  header("Location: index.php");
}
else if(filter_input(INPUT_POST, "nextMonth", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["selectedDate"] = date("Y-m-d", strtotime($_SESSION["selectedDate"]." +1 month"));
  header("Location: index.php");
}
else if(filter_input(INPUT_POST, "submitDate", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["selectedDate"] = date("Y-m-d", strtotime(filter_input(INPUT_POST, "choseDate", FILTER_SANITIZE_SPECIAL_CHARS)));
  header("Location: index.php");
}

include("php/customer.php");
include("php/organizer.php");
//show the calendar
function printCalendar($date){
  $month = date('M', strtotime($date));
  $year = date("Y", strtotime($date));
  $firstDayOfMonth = date("l", strtotime($month."-".$year));
  $lastDayOfMonth = date("t", strtotime($date));
  $firstday = date("N", strtotime($firstDayOfMonth));
  for($i = 1; $i<$lastDayOfMonth+$firstday; $i++){
    if($i % 7 == 1){
      echo "<tr>";
    }
    else if($i % 7 == 7){
      echo "</tr>";
    }
    if($i<$firstday){
      echo "<td class='day'>";
    }
    else if($i){
      echo "<td class='day'>";
      echo "<div class='numberDay'>". (date("d", strtotime($i-$firstday+1 ."-".$month."-".$year)))."</div>";
      echo "<section class='eventsCalendar'>";
      if($_SESSION["role"] == "CUSTOMER"){
        printEventsCustomerCalendar($i-$firstday+1 ."-".$month."-".$year);

      }
      if($_SESSION["role"] == "ORGANIZER"){
        printEventsOrganizerCalendar($i-$firstday+1 ."-".$month."-".$year);

      }
      echo "</section>";
      echo "</td>";
    }
  }
  while($i%7 != 1){
      echo "<td class='day'>";
      echo "</td>";
      $i++;
  }

}


if(filter_input(INPUT_POST, "showMore", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["selectedDate"] = filter_input(INPUT_POST, "date", FILTER_SANITIZE_SPECIAL_CHARS);
  $_SESSION["loadPage"] = "showMore";
  header("Location: index.php");
}

if(filter_input(INPUT_POST, "home", FILTER_SANITIZE_SPECIAL_CHARS)){
    $_SESSION["loadPage"] = NULL;
    $_SESSION["eventToBook"] = NULL;
    header("Location: index.php");
}
?>
