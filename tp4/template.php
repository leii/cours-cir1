<!DOCTYPE HTML>
  <html>
<head>
  <meta charset="utf-8"/>
  <link href="https://fonts.googleapis.com/css?family=Raleway|Roboto" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css"/>
  <title>
    Calendar
  </title>
</head>
<body>
  <?php
    if(isset($_SESSION["role"], $_SESSION["name"])){
      if($_SESSION["role"] == "ORGANIZER"){
        if(isset($_SESSION["loadPage"])){
          if($_SESSION["loadPage"] == "showMore"){
            include("pages/showall.php");
          }
          else if($_SESSION["loadPage"] == "addEvent"){
                  include("pages/addEvent.php");
              }
        }
        else if(isset($_SESSION["eventToBook"])){
          include("pages/book.php");
        }
        else{
          include("pages/host.php");
        }
      }
      else if($_SESSION["role"] == "CUSTOMER"){
          if(isset($_SESSION["loadPage"])){
              if($_SESSION["loadPage"] == "myEvents"){
                  include("pages/myevents.php");
              }
              else if($_SESSION["loadPage"] == "showMore"){
                  include("pages/showall.php");
              }
          }
          else if(isset($_SESSION["eventToBook"])){
              include("pages/book.php");
          }
          else{
              include("pages/client.php");
          }
      }
    }
    else{
      include("pages/form.php");
    }
   ?>
</body>
 </html>
