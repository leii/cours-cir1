<header>
    <form method="POST" action="../controller.php">

    <input type="text" placeholder="name..." name="login" class="menuButton"/>
    <input type="password" placeholder="password..." name="pw" class="menuButton"/>
    <input type="submit" value="LogIn" name="submit" class="menuButton"/>

    </form>
</header>

<?php

if(isset($_SESSION["connectionError"])){
  echo htmlspecialchars($_SESSION["connectionError"]);
}

 ?>
<div id="subForm">
    <h1>Sign in</h1>
    <form method="POST" action="../controller.php">
        <label for="name">Your name:</label>
        <input type="text" name="name" placeholder="Your name..." id="name"/>
        <label for="password">Your password:</label>
        <input type="password" name="pw" placeholder="Your password..." id="password"/>
        <label for="passwordConfirm">Confirm your password:</label>
        <input type="password" name="pwConfirm" placeholder="Confirm your password..." id="passwordConfirm"/>
        <select name="rank">
            <option>CUSTOMER</option>
            <option>ORGANIZER</option>
        </select>
        <input type="submit" value="Sign in" name="signIn"/>
    </form>
</div>
