<header> .
    <form method="POST" action="../controller.php">
        <input type="submit" value="Log out" name="deco" class="menuButton"/>
        <input type="submit" value="Home" name="home" class="menuButton" />
        <input type="submit" value="My events" name="myEvents" class="menuButton"/>
    </form>
</header>
<div id='datePickerMenu'>
    <form method="POST" action="../controller.php">
      <input type="submit" name="previousMonth" id="previousMonth" value="previous"/>
      <section id="chooseDateSection">
        <input type="date" name="choseDate" id="choseDate" value="<?php echo htmlspecialchars(date("Y-m-d", strtotime($_SESSION["selectedDate"])));?>"/>
        <input type="submit" name="submitDate" id="submitDate" value="submit"/>
      </section>
      <input type="submit" name="nextMonth" id="nextMonth" value="next"/>
    </form>
</div>

<table>
  <tr>
    <td>Monday</td>
    <td>Tuesay</td>
    <td>wednesday</td>
    <td>Thursday</td>
    <td>Friday</td>
    <td>Saturday</td>
    <td>Sunday</td>
  </tr>
<?php
  printCalendar($_SESSION["selectedDate"]);
 ?>
</table>
