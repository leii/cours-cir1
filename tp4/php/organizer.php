<?php
//show 5 events of this organizer
function printEventsOrganizerCalendar($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit('Impossible to connect to database.');
  }


  $query = "SELECT * FROM events";
  $statements = $bdd->prepare($query);
  $statements->execute();
  $actualTimestamp = strtotime($date."+1");
  $eventCounter = 1;
  foreach($statements as $row) {
    $startTimestamp = strtotime($row["startdate"])-3600*24;
    $endTimestamp = strtotime($row["enddate"]."+2");
    if($startTimestamp <= $actualTimestamp && $actualTimestamp <= $endTimestamp){
      if($eventCounter <= 5 && $row["organizer_id"] == $_SESSION["id"]){
        $eventCounter += 1;
        echo "<form action='../controller.php' method='POST'>";
        echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' class='eventId' name='eventId'/>";
        echo "<input type='submit' value='".htmlspecialchars($row["name"])."' name='cancel' class='book calendar'/>";
        echo "</form>";


      }
      else if($eventCounter == 6){
        echo "<form class = 'showMoreForm' method='POST' action='../controller.php'>";
        echo "<input type='hidden' value=".htmlspecialchars($date)." name='date' id='date'/>";
        echo "<input type='submit' class='showMore' name='showMore' value='show more'/>";
        echo "</form>";
      }
    }
  }
}

//show all events of a date
function printEventsOrganizerAll($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  }
  catch (Exception $e) {
          exit('Impossible to connect to database.');
  }



  $query = "SELECT * FROM events";
  $statements = $bdd->prepare($query);
  $statements->execute();
  $actualTimestamp = strtotime($date);
  foreach($statements as $row) {
    $startTimestamp = strtotime($row["startdate"])-3600*24;
    $endTimestamp = strtotime($row["enddate"]);
    if($startTimestamp <= $actualTimestamp && $actualTimestamp <= $endTimestamp){
        if($row["organizer_id"] == $_SESSION["id"]){
            echo "<tr>";
            echo "<td class='tableCase'>".htmlspecialchars($row["name"])."</td>";
            echo "<td class='tableCase'>".htmlspecialchars($row["description"])."</td>";
            echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
            echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
            echo "<td class='tableCase'>".htmlspecialchars($row["nb_place"])."</td>";
            echo "<td class='tableCase'>";
            if(file_exists("./images/".$row["id"].".jpg")){
              $type = "jpg";
            }
            else if(file_exists("./images/".$row["id"].".jpeg")){
              $type = "jpeg";
            }
            if(file_exists("./images/".$row["id"].".png")){
              $type = "png";
            }
            if(isset($type)){
                echo "<img src='./images/".$row["id"]."."."$type"."' class='eventImage' alt='error while showing the image'/>";
            }
            echo "</td>";
            echo "<td class='tableCase'>";
            echo "<form action='../controller.php' method='POST'>";
            echo "<input type='hidden' value='".$row["id"]."' id='eventId' name='eventId'/>";
            echo "<input type='submit' value='Cancel' name='cancel' class='cancel'/>";
            echo "</form>";
            echo "</td>";
            echo "</tr>";
        }
    }
  }
}


if(filter_input(INPUT_POST, "submitEvent", FILTER_SANITIZE_SPECIAL_CHARS)){
    if(filter_input(INPUT_POST, "eventName", FILTER_SANITIZE_SPECIAL_CHARS)){
        $name = filter_input(INPUT_POST,"eventName", FILTER_SANITIZE_SPECIAL_CHARS);
        if(filter_input(INPUT_POST, "desc", FILTER_SANITIZE_SPECIAL_CHARS)){
            $desc = filter_input(INPUT_POST,"desc", FILTER_SANITIZE_SPECIAL_CHARS);
            if(filter_input(INPUT_POST, "startdate", FILTER_SANITIZE_SPECIAL_CHARS) && filter_input(INPUT_POST, "starttime", FILTER_SANITIZE_SPECIAL_CHARS)){
                $startdate = (string)filter_input(INPUT_POST, "startdate", FILTER_SANITIZE_SPECIAL_CHARS);
                $starttime = (string)filter_input(INPUT_POST, "starttime", FILTER_SANITIZE_SPECIAL_CHARS);
                $startdate = $startdate." ".$starttime;
                if(filter_input(INPUT_POST, "enddate", FILTER_SANITIZE_SPECIAL_CHARS) && filter_input(INPUT_POST, "endtime", FILTER_SANITIZE_SPECIAL_CHARS)){
                    $enddate = (string)filter_input(INPUT_POST, "enddate", FILTER_SANITIZE_SPECIAL_CHARS);
                    $endtime = (string)filter_input(INPUT_POST, "endtime", FILTER_SANITIZE_SPECIAL_CHARS);
                    $enddate = $enddate." ".$endtime;
                    if(filter_input(INPUT_POST, "places", FILTER_SANITIZE_SPECIAL_CHARS)){
                      $places = filter_input(INPUT_POST,"places", FILTER_SANITIZE_SPECIAL_CHARS);
                      if($_FILES['eventPicture']['type'] == 'image/jpeg' || $_FILES['eventPicture']['type'] == 'image/jpg' || $_FILES['eventPicture']['type'] == 'image/png'){
                          $query = "INSERT INTO events (name,description,startdate,enddate, organizer_id, nb_place) VALUES(:name, :desc, :startdate, :enddate, :id, :places)";
                          $statements = $bdd->prepare($query);
                          $statements->execute([":name"=>$name, ":desc"=>$desc,":startdate"=>$startdate, ":enddate"=>$enddate,":id"=>$_SESSION["id"],":places"=>$places]);
                          $query = "SELECT * FROM events WHERE name = :name AND description = :desc";
                          $statements2 = $bdd->prepare($query);
                          $statements2->execute([":name" => $name, ":desc"=>$desc]);
                          foreach($statements2 as $row){
                            $id = $row["id"];
                          }
                          $type = explode("/",$_FILES["eventPicture"]["type"]);
                          echo $type[1];
                          $imageLink = "./images/".$id.".".$type[1];
                          move_uploaded_file($_FILES["eventPicture"]["tmp_name"], $imageLink);
                          $_SESSION["loadPage"] = NULL;
                      
                        }
                      }
                    }
                 }
            }
        }
         header("Location: index.php");
    }



if(filter_input(INPUT_POST, "addEvent", FILTER_SANITIZE_SPECIAL_CHARS)){
    $_SESSION["loadPage"] = "addEvent";
    header("Location: index.php");
}

if(filter_input(INPUT_POST, "cancel", FILTER_SANITIZE_SPECIAL_CHARS)){
    $id = filter_input(INPUT_POST, "eventId", FILTER_SANITIZE_SPECIAL_CHARS);
    $query = "DELETE FROM user_participates_events WHERE id_event = :id";
    $statements2 = $bdd->prepare($query);
    $statements2->execute([":id"=>$id]);
    $query = "DELETE FROM events WHERE id = :id";
    $statements = $bdd->prepare($query);
    $statements->execute([":id"=>$id]);


    header("Location: index.php");
}
