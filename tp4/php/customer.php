<?php
//show 5 events in a customer's calendar
function printEventsCustomerCalendar($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit('Impossible to connect to database.');
  }

  $query = "SELECT * FROM events";
  $statements = $bdd->prepare($query);
  $statements->execute();
  $actualTimestamp = strtotime($date);
  $eventCounter = 1;
  foreach($statements as $row) {
    $startTimestamp = strtotime($row["startdate"])-3600*24;
    $endTimestamp = strtotime($row["enddate"]);
    if($startTimestamp <= $actualTimestamp && $actualTimestamp <= $endTimestamp){
      if($eventCounter <= 5 && $row["nb_place"] > 0){
        $eventCounter += 1;
        echo "<form action='../controller.php' method='POST'>";
        echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' id='eventId' name='eventId'/>";
        echo "<input type='submit' value='".htmlspecialchars($row["name"])."' name='book' class='book calendar'/>";
        echo "</form>";
      }
      else if($eventCounter == 6){
        echo "<form class = 'showMoreForm' method='POST' action='../controller.php'>";
        echo "<input type='hidden' value=".$date." name='date' id='date'/>";
        echo "<input type='submit' class='showMore' name='showMore' value='show more'/>";
        echo "</form>";
      }
    }
  }
}

//show all events of a date
function printEventsCustomerAll($date){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit('Impossible to connect to database.');
  }


  $query = "SELECT * FROM events";
  $statements = $bdd->prepare($query);
  $statements->execute();
  $actualTimestamp = strtotime($date);
  foreach($statements as $row) {
    $startTimestamp = strtotime($row["startdate"])-3600*24;
    $endTimestamp = strtotime($row["enddate"]);
    if($startTimestamp <= $actualTimestamp && $actualTimestamp <= $endTimestamp){
      if($row["nb_place"] > 0){
        echo "<tr>";
        echo "<td class='tableCase'>".$row["name"]."</td>";
        echo "<td class='tableCase'>".$row["description"]."</td>";
        echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
        echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
        echo "<td class='tableCase'>".htmlspecialchars($row["nb_place"])."</td>";
        echo "<td class='tableCase'>";
        if(file_exists("./images/".$row["id"].".jpg")){
          $type = "jpg";
        }
        else if(file_exists("./images/".$row["id"].".jpeg")){
          $type = "jpeg";
        }
        if(file_exists("./images/".$row["id"].".png")){
          $type = "png";
        }
        if(isset($type)){
            echo "<img src='./images/".$row["id"]."."."$type"."' class='eventImage' alt='error while showing the image'/>";
        }
        echo "</td>";
        echo "<td class='tableCase'>";
        echo "<form action='../controller.php' method='POST'>";
        echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' id='eventId' name='eventId'/>";
        echo "<input type='submit' value='Book' name='go' class='go'/>";
        echo "</form>";
        echo "</td>";
        echo "</tr>";
      }
    }
  }
}


//book pages
function printBook($id){
  $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit('Impossible to connect to database.');
  }

  $query = "SELECT * from events WHERE id= :id";
  $statements = $bdd->prepare($query);
  $statements->execute([":id" => $id]);
  foreach ($statements as $row) {
    if($row["nb_place"] > 0){
      echo "<tr>";
      echo "<td class='tableCase'>".htmlspecialchars($row["name"])."</td>";
      echo "<td class='tableCase'>".htmlspecialchars($row["description"])."</td>";
      echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
      echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
      echo "<td class='tableCase'>".htmlspecialchars($row["nb_place"])."</td>";
      echo "<td class='tableCase'>";
      if(file_exists("./images/".$row["id"].".jpg")){
        $type = "jpg";
      }
      else if(file_exists("./images/".$row["id"].".jpeg")){
        $type = "jpeg";
      }
      if(file_exists("./images/".$row["id"].".png")){
        $type = "png";
      }
      if(isset($type)){
          echo "<img src='./images/".$row["id"]."."."$type"."' class='eventImage' alt='error while showing the image'/>";
      }
      echo "</td>";
      echo "<td class='tableCase'>";
      echo "<form action='../controller.php' method='POST'>";
      echo "<input type='hidden' value='".htmlspecialchars($row["id"])."' id='eventId' name='eventId'/>";
      echo "<input type='submit' value='Book' name='go' class='go'/>";
      echo "</form>";
      echo "</td>";
      echo "</tr>";
    }
  }
}

//show all reserved events
function myIncEvents(){
    $ini_array = parse_ini_file("secrets.ini",true);
  try {
     $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
     $bdd = new PDO($ini_array['db']['dsn'],$ini_array['db']['user'], $ini_array['db']['pass'], $opts);
  } catch (Exception $e) {
          exit('Impossible to connect to database.');
  }

  $query = "SELECT * FROM events INNER JOIN user_participates_events ON user_participates_events.id_event = events.id WHERE user_participates_events.id_participant = :id";
  $statements = $bdd->prepare("$query");
  $statements->execute([":id" => $_SESSION["id"]]);
  foreach($statements as $row){
      echo "<tr>";
      echo "<td class='tableCase'>".htmlspecialchars($row["name"])."</td>";
      echo "<td class='tableCase'>".htmlspecialchars($row["description"])."</td>";
      echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["startdate"])))."</td>";
      echo "<td class='tableCase'>".htmlspecialchars(date("Y-m-d", strtotime($row["enddate"])))."</td>";
      echo "</tr>";
  }
}


if(filter_input(INPUT_POST, "home", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["showmore"] = NULL;
  $_SESSION["eventToBook"] = NULL;
  header("Location: index.php");
}

if(filter_input(INPUT_POST, "book", FILTER_SANITIZE_SPECIAL_CHARS)){
  $_SESSION["eventToBook"] = (string)filter_input(INPUT_POST, "eventId", FILTER_SANITIZE_SPECIAL_CHARS);
  header("Location: index.php");
}

//add person to event
if(isset($_POST["go"])){
  //add person to event
  $query = "INSERT INTO user_participates_events (id_participant, id_event) VALUES(:id_participant, :id_events)";
  $statements = $bdd->prepare($query);
  $statements->execute([":id_participant" => $_SESSION['id'], ":id_events" => $_POST['eventId']]);

  //remove one place from availble places
  $query = "SELECT nb_place FROM events where id= :id";
  $statements = $bdd->prepare($query);
  $statements->execute([":id" => $_POST["eventId"]]);
  foreach ($statements as $row) {
    $nbPlace = $row["nb_place"];
  }

  $query = "UPDATE events SET nb_place = :nbPlace WHERE id = :id";
  $statements = $bdd->prepare($query);
  $statements->execute([":nbPlace" => $nbPlace-1, ":id" => $_POST["eventId"]]);
  $_SESSION["showmore"] = NULL;
  $_SESSION["eventToBook"] = NULL;
  header("Location: index.php");

}

//show all events of connected person
if(isset($_POST["myEvents"])){
    $_SESSION["loadPage"] = "myEvents";
    header("Location: index.php");
}
?>
