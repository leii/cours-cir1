<html>
<head>
  <meta charset="utf-8"/>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <div id="form">
    <form method="POST" action="index.php">
      <label for="pseudo">Pseudo: </label><input type="text" name="name" id="name"/>
      <input type="text" id="message" name="message" placeholder="Votre message..."/>
      <input type="submit" name="submit" id="submit" value="submit"/>
    </form>
  </div>
  <div id="chat">
    <?php
    if(isset($bdd)){
      $messages = $bdd->query("SELECT * FROM messages ORDER BY id DESC");
      foreach($messages as $myMessage){
        echo "<br/><br/><div class='message'>";
        $modifiedMessage = explode("/me ", $myMessage["message"]);
        if(isset($modifiedMessage[1])){
          echo htmlspecialchars($myMessage["sendDate"])." - ".htmlspecialchars($myMessage["autor"]).": <div class='modifiedMessage'>".htmlspecialchars($modifiedMessage[1])."</div>";
        }
        else{
          echo htmlspecialchars($myMessage["sendDate"])." - ".htmlspecialchars($myMessage["autor"]).": ".$myMessage["message"];
        }
        echo "</div>";
      }
    }

    ?>
  </div>
</body>

</html>
